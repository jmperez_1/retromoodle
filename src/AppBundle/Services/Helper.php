<?php
namespace AppBundle\Services;

use AppBundle\Entity\MemberActivity;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class Helper
{

    // Registrar en la tabla activity que este usuario ha querido app_member_edit la contraseña
    public function memberActivity($em, $keyActivity, $idMember)
    {
        $created = new \Datetime("now");

        $memberActivity = new MemberActivity();

        $memberActivity->setKeyActivity($keyActivity);

        $idMember = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMember));
        $memberActivity->setIdMember($idMember);

        $memberActivity->setFecha($created);
        $em->persist($memberActivity);
        $em->flush();
    }

    // public function securityAccess() {
    //     if (
    //         $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY')
    //         || $this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')
    //         || $this->get('security.authorization_checker')->isGranted('ROLE_PROFESIONAL')
    //     ){} else {
    //         throw new AccessDeniedException();
    //     }
    // }

}

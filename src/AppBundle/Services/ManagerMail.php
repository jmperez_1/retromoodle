<?php
namespace AppBundle\Services;

class ManagerMail {
	
	public function templeteOne($value){
        $em = $value['em'];
        $to = $value['to'];
        $localeMember = $value['localeMember'];
        $subject = $value['subject'];
        $content = $value['content'];

        $smtpHost = $em->getRepository('AppBundle:General')->findOneBy(array("keyGlobal" => "smtp_host"));
        $smtpPort = $em->getRepository('AppBundle:General')->findOneBy(array("keyGlobal" => "smtp_port"));
        $smtpEncrypt = $em->getRepository('AppBundle:General')->findOneBy(array("keyGlobal" => "smtp_encrypt"));
        $smtpMail = $em->getRepository('AppBundle:General')->findOneBy(array("keyGlobal" => "smtp_mail"));
        $smtpMailName = $em->getRepository('AppBundle:General')->findOneBy(array("keyGlobal" => "smtp_mail_name"));
        $smtpPassword = $em->getRepository('AppBundle:General')->findOneBy(array("keyGlobal" => "smtp_password"));

        $transport = \Swift_SmtpTransport::newInstance($smtpHost->getValue(), $smtpPort->getValue(), $smtpEncrypt->getValue())->setUsername($smtpMail->getValue())->setPassword($smtpPassword->getValue());
        $mailer = \Swift_Mailer::newInstance($transport);
        $message = \Swift_Message::newInstance($subject)
            ->setFrom(array($smtpMail->getValue() => $smtpMailName->getValue()))
            ->setTo($to)
            ->setBody($content, "text/html");

        $mailer->send($message);

        return 1;
    }
}


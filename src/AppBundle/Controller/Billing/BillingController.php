<?php

namespace AppBundle\Controller\Billing;

use AppBundle\Entity\Member;
use AppBundle\Entity\MemberMeta;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class BillingController extends Controller
{

    /**
     * @Route("/{_locale}/billing/add-to-cart")
     */
    public function addToCartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prod = $this->get("app.services_product");

        $idProduct = $request->request->get('idProduct');
        $finished = $request->request->get('finshed');
        $idMember = $this->get('security.token_storage')->getToken()->getUser()->getIdMember();
        
        $valores = array(
            "em" => $em,
            "localeProduct" => 'es',
            "idProduct" => $idProduct,
            "filtreMinPrice" => null,
            "filtreMaxPrice" => null,
            "filtreCategory" => null,
        );
        $product = $prod->searchProduct($valores);
        $product = json_encode($product, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK);

        $billing = $em->getRepository('AppBundle:Billing')->findOneBy(array('idMember' => $idMember, 'finished' => 0));

        $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMember, 'deleted' => 0));
        
        $detailsMember = array(
            "name" => $member->getName(),
            "username" => $member->getUsername(),
            "apellido" => $member->getApellido()
        );
        $detailsMember = json_encode($detailsMember, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK);
        
        $response = new JsonResponse();

        if ($billing){
            $billing->setIdMember($member);
            $billing->setFacturationNumber($idProduct);
            $billing->setCreationDate(new DateTime('now'));
            $billing->setDetailsMember($detailsMember);

            if ($billing->getProduct()) {
                $billing->setProduct($billing->getProduct() . $product);
            } else {
                $billing->setProduct($product);
            }

            if ($finished == 1) {
                $billing->setFinished(1);
                $billing->setFinalitzationDate(new DateTime('now'));
            } else {
                $billing->setFinished(0);
            }

            //dump($billing);
            //die();

            // $em->persist($billing);
            // $em->flush();

            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Anadido el producto correctamente a el carro.",
            ));
            
        } else {
            $response->setStatusCode(500);
            $response->setData(array(
                'response' => 'error',
                'msg' => "No se pudo anadir el producto a el carro.",
            ));
        }

        return $response;
    }

}

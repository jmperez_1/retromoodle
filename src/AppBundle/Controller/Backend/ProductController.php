<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductMeta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProductController extends Controller
{

    /**
     * @Route("/{_locale}/panel/product/gestion")
     */
    public function gestionAction(Request $request)
    {
        $prod = $this->get("app.services_product");

        $em = $this->getDoctrine()->getManager();

        $localeProduct = $request->get("localeProduct");
        $filtreMinPrice = $request->get("filtreMinPrice");
        $filtreMaxPrice = $request->get("filtreMaxPrice");
        $filtreCategory = $request->get("category");

        if (!$localeProduct) {
            $localeProduct = $request->getLocale();
        }
        $valores = array(
            "em" => $em,
            "localeProduct" => $localeProduct,
            "idProduct" => null,
            "filtreMinPrice" => $filtreMinPrice,
            "filtreMaxPrice" => $filtreMaxPrice,
            "filtreCategory" => $filtreCategory,
        );
        $product = $prod->searchProduct($valores);

        //Aplicació del bundle KnpPaginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $product, /* query NOT result */
            $request->query->getInt('page', 1) /*page number*/,
            10/*limit per page*/
        );

        $category = $em->getRepository('AppBundle:Category')->findBy(array('categoryKey' => 'product_level_1'));

        return $this->render('backend/product/gestion.html.twig', array(
            'pagination' => $pagination,
            'filtreMinPrice' => $filtreMinPrice,
            'filtreMaxPrice' => $filtreMaxPrice,
            'category' => $category,
        ));
    }

    /**
     *@Route("/{_locale}/panel/product/edit/{idProduct}", defaults={"idProduct":null})
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prod = $this->get("app.services_product");
        $idProduct = $request->get('idProduct');
        $localeProduct = $request->get("localeProduct");
        $categoryPhotosArray = array();
        $categoryPhotosJSON = array();

        if (!$localeProduct) {
            $localeProduct = $request->getLocale();
        }

        $category = $em->getRepository('AppBundle:Category')->findBy(array('categoryKey' => 'product_level_1', 'active' => 1));
        $categoryPhotos = $em->getRepository('AppBundle:Category')->findBy(array('categoryKey' => 'product_photo', 'active' => 1));

        for ($a = 0; $a < count($categoryPhotos); $a++) {
            array_push($categoryPhotosArray, json_decode($categoryPhotos[$a]->getName(), true));
            array_push($categoryPhotosJSON, json_decode($categoryPhotos[$a]->getName(), true));
        }

        $categoryPhotosJSON = json_encode($categoryPhotosJSON, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK);

        $totalPhotos = null;
        $photos = null;
        if ($idProduct) {
            $valores = array(
                "em" => $em,
                "localeProduct" => $localeProduct,
                "idProduct" => $idProduct,
                "filtreMinPrice" => null,
                "filtreMaxPrice" => null,
                "filtreCategory" => null,
            );
            $product = $prod->searchProduct($valores);
            if (!$product) {
                $valores = array(
                    "em" => $em,
                    "localeProduct" => 'es',
                    "idProduct" => $idProduct,
                    "filtreMinPrice" => null,
                    "filtreMaxPrice" => null,
                    "filtreCategory" => null,
                );
                $product = $prod->searchProduct($valores);
                $product[0]['name'] = '';
                $product[0]['url'] = '';
            }
            if ($product[0]['photos']) {
                $totalPhotos = count($product[0]['photos']);
                $photos = json_encode($product[0]['photos'], JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK);
            }
        } else {
            $product = array(
                "0" => array(
                    "idProduct" => '',
                    "price" => '',
                    "stock" => '',
                    "deleted" => '',
                    "visible" => '',
                    "category" => '',
                    "locale" => '',
                    "name" => '',
                    "url" => '',
                    "photos" => '',
                ),
            );
        }

        return $this->render('backend/product/edit.html.twig', array(
            "product" => $product,
            "category" => $category,
            "categoryPhotos" => $categoryPhotosArray,
            "localeProduct" => $localeProduct,
            "totalPhotos" => $totalPhotos,
            "photos" => $photos,
            "categoryPhotosJSON" => $categoryPhotosJSON,
            "idProduct" => $idProduct,
        ));
    }
    /**
     * @Route("/api/panel/product/update")
     */
    public function apiUpdateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $photos = $request->request->get('array');
        $idProduct = $request->request->get('idProduct');
        $product = $em->getRepository('AppBundle:Product')->findOneBy(array('idProduct' => $idProduct));
        $arrayPhoto = json_decode($photos, true);
        
        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "No se puede actualizar el producto, revisa la información proporcionada por favor."
        ));
        
        try{
            // ### PASO 1 Eliminar las fotos
            $temp = array();
            for ($a = 0; $a < count($arrayPhoto); $a++) {
                $existNew = 0;
                $existDeleted = 0;
                foreach ($arrayPhoto[$a] as $key => $item_value) {
                    if ($key == "deleted") {$existDeleted = 1;}
                    if ($key == "new") {$existNew = 1;}
                }
                if ($existDeleted == 1) {
                    if ($existNew == 0) {
                        unlink("product/$idProduct/photos/" . $arrayPhoto[$a]["name"]);
                    }
                } else {
                    array_push($temp, $arrayPhoto[$a]);
                }
            }
            $arrayPhoto = $temp;
            // END PASO 1 Eliminar las fotos

            // ### PADO 2 ### Guardar photo
            for ($a = 0; $a < count($arrayPhoto); $a++) {
                $existNew = 0;
                $existBase64 = 0;
                foreach ($arrayPhoto[$a] as $key => $item_value) {
                    if ($key == "new") {$existNew = 1;}
                    if ($key == "base64") {$existBase64 = 1;}
                }
                if ($existBase64 == 1) {
                    $ruta = "product/" . $idProduct . "/photos/";
                    if ($existNew == 1) {
                        //comprueba existe el directorio y si no lo crea
                        if (!file_exists($ruta)) {mkdir($ruta, 0777, true);}
                        unset($arrayPhoto[$a]["new"]);
                    } else {
                        unlink("product/$idProduct/photos/" . $arrayPhoto[$a]["name"]);
                    }
                    $numeroAleatorio = rand(1, 1000000);
                    $nombre = time() . "-" . $numeroAleatorio . ".png";
                    // RECOGE LA IMAGEN I LA GUARDA
                    $base_to_php = explode(',', $arrayPhoto[$a]["base64"]);
                    $data = base64_decode($base_to_php[1]);
                    $filepath = $ruta . $nombre;
                    file_put_contents($filepath, $data);
                    // END RECOGE LA IMAGEN I LA GUARDA
                    //MODIFIQUEM $arrayPhoto
                    unset($arrayPhoto[$a]["base64"]);
                    $arrayPhoto[$a]["name"] = $nombre;
                }
            }
            // END PADO 2 Guardar photo

            //reindexem l'array amb les fotografies
            $photos = array_values($arrayPhoto);
            // Convertir category a json para base de datos "string"
            $photos = json_encode($photos, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK);

            // dump($photos);

            $product->setPhotos($photos);
            $em->persist($product);
            $em->flush();
            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Producto actualizado correctamente."
            ));
        }catch(Exception $e){
            $response->setStatusCode(500);
            $response->setData(array(
                'response' => 'error',
                'msg' => "No se puede actualizar el producto, revisa la información proporcionada por favor."
            ));
        }
        
        return $response;
    }
    /**
     * @Route("/api/panel/product/save")
     */
    public function apiSaveAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "No se puede actualizar el producto, revisa la información proporcionada por favor."
        ));

        try{
            $idMemberActual = $this->get('security.token_storage')->getToken()->getUser()->getIdMember();
            // Product
            $idProduct = $request->request->get('idProduct');
            $price = $request->request->get('price');
            $stock = $request->request->get('stock');
            $category = $request->request->get('category');
            $visible = $request->request->get('visible');
            $deleted = $request->request->get('deleted');
            //crec que aquestes dues no son necessaries a apartir d'ara per la
            //gestió de les fotografies
            $photoToDelete = $request->request->get('photoToDelete');
            $photo = $request->files->get('photo');

            // Convertir category a json para base de datos "string"
            $category = json_encode($category, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK);

            // ProductMeta
            $url = $request->request->get('url');
            $name = $request->request->get('name');
            $localeProduct = $request->request->get('localeProduct');

            // Product
            $product = $em->getRepository('AppBundle:Product')->findOneBy(array('idProduct' => $idProduct));

            //Member
            $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMemberActual));

            $memberAll = $em->getRepository('AppBundle:Member')->findAll();

            $helper = $this->get("app.helper");
            if (!$product) {
                $product = new Product();
                $helper->memberActivity($em, 'product_created', $memberAll[0]['id_Member']);
            } else {
                $helper->memberActivity($em, 'product_updated', $memberAll[0]['id_Member']);
            }

            $product->setPrice($price);
            $product->setStock($stock);
            $product->setCategory($category);
            $product->setIdMember($member);
            $product->setVisible($visible);
            $product->setDeleted($deleted);
        
            $em->persist($product);
            $em->flush();
        
            $em->persist($product);
            $em->flush();

            //obtenim idProduct en cas que s'estigui inserint un producte nou
            if ($idProduct == "") {$idProduct = $product->getIdProduct();}
            //
            // ProductMeta
            $productMeta = $em->getRepository('AppBundle:ProductMeta')->findOneBy(array('idProduct' => $idProduct, 'locale' => $localeProduct));

            if (!$productMeta) {
                $productMeta = new ProductMeta();
                $productMeta->setIdProduct($product);
                $productMeta->setLocale($localeProduct);
            }

            $productMeta->setUrl($url);
            $productMeta->setName($name);

            $em->persist($productMeta);
            $em->flush();
            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'idProduct' =>$product->getIdProduct()
            ));
        }catch(Exception $e){
            $response->setStatusCode(500);
            $response->setData(array(
                'response' => 'error',
                'msg' => "No se puede actualizar el producto, revisa la información proporcionada por favor."
            ));
        }
        
        return $response;
    }
}

<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Entity\Member;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MemberController extends Controller
{
    /**
     * @Route("/{_locale}/panel/member/edit/{idMember}", defaults={"_locale": "es"})
     */
    public function editAction(Request $request)
    {
        //doctrine
        $em = $this->getDoctrine()->getManager();
        $idMember = $request->get('idMember');

        $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMember));
        $memberMeta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('idMember' => $idMember));

        //USUARIO logueado
        //$member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $id, 'deleted' => '0'));
        $idMemberActual = $this->get('security.token_storage')->getToken()->getUser()->getIdMember();
        if ($member->getIdMember() != $idMemberActual && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
            throw new AccessDeniedException();
        }
        //

        return $this->render('backend/member/edit.html.twig', array(
            "member" => $member,
            "memberMeta" => $memberMeta,
        ));
    }
    /**
     * @Route("/panel/api/member/edit")
     */
    public function apiEditAction(Request $request, \Swift_Mailer $mailer)
    {
        //doctrine
        $em = $this->getDoctrine()->getManager();
        $manager_mail = $this->get("app.manager_mail");

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "No se pudo actualizar el usuario."
        ));

        try{

            $idMember = $request->request->get('idMember');
            $name = $request->request->get('name');
            $surname = $request->request->get('surname');
            $surname2 = $request->request->get('surname2');
            $bday = $request->request->get('bday');
            $password = $request->request->get('password');
            $password_rep = $request->request->get('password_rep');
            $prefijo = $request->request->get('prefijo');
            $username = $request->request->get('username');
            $gender = $request->request->get('gender');
            $valido = $request->request->get('validado');
            $delete = $request->request->get('deleted');

            //
            $file = $request->files->get('upload');
            $status = array('status' => "success", "fileUploaded" => false);
            //select a bbdd
            $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMember));

            //USUARIO logueado
            //$member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $id, 'deleted' => '0'));
            $idMemberActual = $this->get('security.token_storage')->getToken()->getUser()->getIdMember();
            if ($member->getIdMember() != $idMemberActual && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
                throw new AccessDeniedException();
            }
            //

            $member->setName($name);
            $member->setApellido($surname);
            $member->setApellido2($surname2);
            $member->setFechaNacimiento(new DateTime($bday));
            //

            // AVATAR
            if (!is_null($file)) {
                $avatar = $member->getAvatar();
                if ($avatar != null) {
                    unlink("member/$idMember/avatar/" . $avatar);
                }
                // generate a random name for the file but keep the extension
                $filename = uniqid() . "." . $file->getClientOriginalExtension();
                $path = "member/$idMember/avatar/";
                $file->move($path, $filename); // move the file to a path
                $member->setAvatar($filename);
                $status = array('status' => "success", "fileUploaded" => true);
            }
            //  END AVATAR

            // Cifrar la password
            if ($password && $password == $password_rep) {
                $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
                $member->setSalt($random_salt);
                //codificació password
                $passwordCode = hash('sha512', $password);
                $pwd = hash('sha512', $passwordCode . $random_salt);
                $member->setPassword($pwd);
            }

            // END Cifrar la password

            // Control canvi username
            if ($username != $member->getUsername()) {
                $llistat_emails = $em->getRepository('AppBundle:Member')->findOneBy(array('username' => $username));
                if ($llistat_emails) {
                    $status = "error_username";
                } else {
                    $member->setVerificadoEmail(0);
                    $member->setUsername($username);
                    $value = array(
                        "em" => $em,
                        "localeMember" => $request->getLocale(),
                        "to" => $username,
                        "subject" => "OFFING: Email de verificación",
                        "content" => 
                            $this->renderView("templates/emails/member/verification_mail.html.html.twig", array(
                                'idMember' => $idMember, 
                                'member' => $member
                            ))
                    );
                    $manager_mail->templeteOne($value);
                    $status = "ok";
                }
            }
            // END Control canvi username

            //enviar keyActivity
            $memberAll = $em->getRepository('AppBundle:Member')->findAll();
            $helper = $this->get("app.helper");
            $helper->memberActivity($em, 'member_modified', $memberAll[0]['id_Member']);
            //END enviar keyActivity
            
            $em->persist($member);
            $em->flush();

            $memberMeta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('idMember' => $idMember));

            $memberMeta->setGender($gender);
            $em->persist($memberMeta);
            $em->flush();
            $response->setStatusCode(200);
            if($status==="error_username"){
                $response->setData(array(
                    'response' => 'error_username',
                    'msg' => "Ya existe un usuario registrado con este correo electronico"
                ));
            }else{                
                $response->setData(array(
                    'response' => 'success',
                    'msg' => "Usuario actualizado correctamente."
                ));
            }
        }catch(Exception $e){
            $response->setStatusCode(500);
            $response->setData(array(
                'response' => 'error',
                'msg' => "No se pudo actualizar el usuario."
            ));
        }
        return $response;
        //return new Response(1);
    }
    /**
     * @Route("/panel/api/member/delete")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function apiDeleteAction(Request $request)
    {
        $idMember = $request->request->get('idMember');

        $em = $this->getDoctrine()->getManager();
        $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMember, 'deleted' => 0));

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "No se pudo eliminar el usuario."
        ));
        if ($member) {
        
            //Calculamos la fecha de BAJA
            $member->setDeactivationDate(new DateTime('now'));
            $member->setDeleted(1);
            $em->persist($member);
            $em->flush();
    
            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'successs',
                'msg' => "El usuario se ha eliminado correctamente."
            ));
        }
        
        return $response;

    }

    /**
     * @Route("/verificar-email/{idMember}/{secretCode}")
     */
    public function verificarEmailAction(Request $request)
    {
        //doctrine
        $em = $this->getDoctrine()->getManager();
        $idMember = $request->get('idMember');
        $secret_code = $request->get('secretCode');

        $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $idMember));
        $member_meta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('idMember' => $idMember));
        if ($member && $member_meta->getEmailSecretCode() == $secret_code) {
            $member->setVerificadoEmail(1);
            $member_meta->setEmailSecretCode(null);
            $em->persist($member);
            $em->flush();
            $em->persist($member_meta);
            $em->flush();
        } else {
            dump('el codi secret no coincideix');
        }
        #mirar com reedirigir a una altra pàgina
        return $this->redirectToRoute('app_backend_member_edit', array('idMember' => $idMember));
    }

    /**
     * @Route("/{_locale}/panel", defaults={"_locale": "es"})
     */
    public function panelAction(Request $request)
    {
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->render('backend/member/panel.html.twig');
    }

    /**
     * @Route("/{_locale}/panel/member/gestion", defaults={"_locale": "es"})
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function gestionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $filterUsername = $request->get('filterUsername');
        $filterRole = $request->get("filterRole");
        $filterName = $request->get("filterName");

        $arrayWhere = array();

        if ($filterUsername) {
            $arrayWhere["username"] = $filterUsername;
        }
        if ($filterRole) {
            $arrayWhere["idMemberRole"] = $filterRole;
        }
        if ($filterName) {
            $arrayWhere["name"] = $filterName;
        }
        $arrayWhere["deleted"] = false;

        $member = $em->getRepository('AppBundle:Member')->findBy($arrayWhere);
        $memberAll = $em->getRepository('AppBundle:Member')->findAll();

        // dump($member);
        // die();

        //$member = $em->getRepository('AppBundle:Member')->findAll();

        //Aplicació del bundle KnpPaginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $member, /* query NOT result */
            $request->query->getInt('page', 1) /*page number*/,
            2/*limit per page*/
        );

        return $this->render('backend/member/gestion.html.twig', array(
            'pagination' => $pagination,
            'member' => $member,
            'memberAll' => $memberAll,
            'filterRole' => $filterRole,
            'filterName' => $filterName,
        ));

    }

}

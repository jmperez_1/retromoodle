<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{
    /**
     * @Route("/default/pruebas")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function pruebasAction(Request $request)
    {
        echo "prueba";
        die();
    }
}

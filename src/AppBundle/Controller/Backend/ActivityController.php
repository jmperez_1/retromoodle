<?php

namespace AppBundle\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ActivityController extends Controller
{
    /**
     * @Route("/{_locale}/panel/activity/gestion", defaults={"_locale": "es"})
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function gestionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $filterActivityType = $request->get('filterActivityType');
        $filterEmail = $request->get('filterEmail');

        $arrayWhere = array();

        if ($filterActivityType) {
            $arrayWhere["keyActivity"] = $filterActivityType;
        }
        if ($filterEmail) {
            $arrayWhere["idMember"] = $filterEmail;
        }
        $tipoActivity = $em->getRepository('AppBundle:MemberActivity')->findBy($arrayWhere);

        //Aplicació del bundle KnpPaginator
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $tipoActivity, /* query NOT result */
            $request->query->getInt('page', 1) /*page number*/,
            2/*limit per page*/
        );

        $memberAll = $em->getRepository('AppBundle:Member')->findAll();
        $tipoActivityAll = $em->getRepository('AppBundle:MemberActivity')->findAll();

        $keyActivity = array_map(function ($obj) {return $obj->getKeyActivity();}, $tipoActivityAll);

        $keyActivity = array_unique($keyActivity);

        

        return $this->render('backend/activity/gestion.html.twig', array(
            'pagination' => $pagination,
            'memberAll' => $memberAll,
            'tipoActivity' => $tipoActivity,
            'tipoActivityAll' => $tipoActivityAll,
            'filterActivityType' => $filterActivityType,
            'keyActivity' => $keyActivity,
        ));
    }

}

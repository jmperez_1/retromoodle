<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Entity\Member;
use AppBundle\Entity\MemberMeta;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class MemberController extends Controller
{
    /**
     * @Route("/member/login-check")
     */
    public function loginCheckAction(Request $request)
    {
        return new response(true);
    }

    /**
     * @Route("/member/logout")
     */
    public function logoutAction(Request $request)
    {
        return new response(true);
    }

    /**
     * @Route("/{_locale}", defaults={"_locale": "es"})
     */
    public function indexAction(Request $request)
    {
        return $this->render('frontend/member/index.html.twig');
    }

    /**
     * @Route("/{_locale}/member/register", defaults={"_locale": "es"})
     */
    public function registerAction(Request $request)
    {
        return $this->render('frontend/member/register.html.twig');
    }

    /**
     * @Route("/{_locale}/member/recuperar_contraseña", defaults={"_locale": "es"})
     */
    public function recuperarContraseña(Request $request)
    {
        return $this->render('frontend/member/recuperar_contraseña.html.twig');
    }

    /**
     * @Route("/{_locale}/member/perfil_alumno", defaults={"_locale": "es"})
     */
    public function perfilAlumno(Request $request)
    {
        return $this->render('frontend/member/perfil_alumno.html.twig');
    }

    /**
     * @Route("/{_locale}/member/perfil_profesor", defaults={"_locale": "es"})
     */
    public function perfilProfesor(Request $request)
    {
        return $this->render('frontend/member/perfil_profesor.html.twig');
    }

    /**
     * @Route("/{_locale}/api/member/login")
     */
    public function apiLoginAction(Request $request)
    {
        $helper = $this->get("app.helper");

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "Hubo un error al iniciar sesión, por favor revisa los campos y vuelve a intentarlo.",
        ));

        $username = $request->request->get('_username');
        $password = $request->request->get('_password');

        $em = $this->getDoctrine()->getManager();

        $member = $em->getRepository('AppBundle:Member')->findUserLogin($username);
        if ($member) {
            $password = hash('sha512', $password);
            $pwd = hash('sha512', $password . $member[0]['salt']);

            $member = $em->getRepository('AppBundle:Member')->findUserLoginDos($username, $pwd);
            
            if ($member) {
                // es correcto
                // validamos el usuario, por que a escrito correctamente el pass
                if ($member[0]['verificado_email'] == 1) {

                    $memberTres = $em->getRepository('AppBundle:Member')->checkExist($username);
                    $helper->memberActivity($em, 'login_attempt_success', $memberTres[0]['id_Member']);

                    $response->setStatusCode(200);
                    $response->setData(array(
                        'response' => 'success',
                        'msg' => "Paso a validación.",
                    ));

                    return $response;

                } else {

                    $response->setStatusCode(500);
                    $response->setData(array(
                        'response' => 'error',
                        'msg' => "Por favor verifica tu e-mail para iniciar sesión.",
                    ));
                    return $response;

                }
            }

        }

        // # NO es correcto
        // Tipo de activity
        $member = $em->getRepository('AppBundle:Member')->checkExist($username);
        if ($member) {
            $helper->memberActivity($em, 'login_attempt_fail', $member[0]['id_Member']);
        }
        return $response;
    }

    /**
     * @Route("/{_locale}/api/member/registrar")
     */
    public function apiRegistrarAction(Request $request)
    {
        $helper = $this->get("app.helper");
        $manager_mail = $this->get("app.manager_mail");

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "Hubo un error al registrarte.",
        ));

        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $name = strstr($username, '@', true);

        $member = $this->getDoctrine()->getManager()->getRepository('AppBundle:Member')
            ->checkExist($username);

        if (!$member) {
            $member = new Member();

            $memberRole = $em->getRepository('AppBundle:MemberRole')->findOneBy(array('idMemberRole' => 3));
            $member->setIdMemberRole($memberRole);

            $member->setName($name);
            $member->setUsername($username);
            $member->setCreationDate(new DateTime('now'));
            $member->setVerificadoEmail(0);
            $member->setValidated(0);
            $member->setDeleted(0);

            //Cifrar la password
            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $member->setSalt($random_salt);

            $passwordCode = hash('sha512', $password);
            $pwd = hash('sha512', $passwordCode . $random_salt);
            $member->setPassword($pwd);
            //

            $em->persist($member);
            $em->flush();

            $meta = new MemberMeta();
            $meta->setIdMember($member);
            $em->persist($meta);
            $em->flush();

            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $urlCode = hash('sha512', 'memberactivationoffing' . $member->getIdMember($member));
            $url = hash('sha512', $urlCode . $random_salt);

            $meta->setEmailSecretCode($url);
            $em->persist($meta);
            $em->flush();

            $value = array(
                "em" => $em,
                "localeMember" => $request->getLocale(),
                "to" => $username,
                "subject" => "Te facilitamos tus datos de acceso",
                "content" =>
                $this->renderView("templates/emails/member/register_member.html.twig", array(
                    "text" => "Accede al siguiente link para activar tu cuenta:",
                    "url" => $this->generateUrl('app_frontend_member_apiactivate', array(
                        'secretcode' => $meta->getEmailSecretCode(),
                    )),
                )),
            );
            
            $manager_mail->templeteOne($value);

            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Enhorabuena, te has registrado correctamente. Comprueba el correo para verificar el registro!",
            ));
        }
        return $response;
    }

    /**
     * @Route("/{_locale}/api/member/activate/{secretcode}")
     */
    public function apiActivateAction(Request $request, $secretcode, \Swift_Mailer $mailer)
    {
        $manager_mail = $this->get("app.manager_mail");

        $em = $this->getDoctrine()->getManager();
        $validado = 0;
        $expired = 0;
        $memberMeta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('emailSecretCode' => $secretcode));
        if ($memberMeta) {
            $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $memberMeta->getIdMember()->getIdMember()));

            if ($member) {

                $now = new DateTime('now');
                $registered = $member->getCreationDate(1);
                $registered = $registered->getTimestamp();
                $now = $now->getTimestamp();
                if ($now - $registered <= 259200) {
                    $member->setVerificadoEmail(1);
                    $member->setValidated(1);
                    $em->persist($member);
                    $em->flush();

                    $memberMeta->setEmailSecretCode(null);
                    $em->persist($memberMeta);
                    $em->flush();

                    $validado = 1;
                } else {
                    $expired = 1;
                }

            }

        }

        if ($validado === 1) {
            return $this->render('frontend/member/validate_ok.html.twig');
        } else {
            return $this->render('frontend/member/validate_wrong.html.twig', array("expired" => $expired));
        }
    }

    /**
     * @Route("/{_locale}/member/resend")
     */
    public function activateResendAction(Request $request, \Swift_Mailer $mailer)
    {
        return $this->render('frontend/member/validate_resend.html.twig');
    }

    /**
     * @Route("/{_locale}/api/member/resend-activation")
     */
    public function apiResendAction(Request $request, \Swift_Mailer $mailer)
    {
        $helper = $this->get("app.helper");
        $manager_mail = $this->get("app.manager_mail");

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "Hubo un error al reenviar el email, vuelve a intentarlo más tarde.",
        ));

        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('emailUser');

        $member = $this->getDoctrine()->getManager()->getRepository('AppBundle:Member')->findOneBy(
            array('username' => $username, 'verificadoEmail' => '0', 'validated' => '0', 'deleted' => '0')
        );

        if ($member) {
            $meta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('idMember' => $member->getIdMember()));

            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $urlCode = hash('sha512', 'memberactivationoffing' . $member->getIdMember($member));
            $url = hash('sha512', $urlCode . $random_salt);
            $meta->setEmailSecretCode($url);
            $member->setCreationDate(new DateTime('now'));
            $em->persist($meta);
            $em->persist($member);
            $em->flush();

            $value = array(
                "em" => $em,
                "localeMember" => $request->getLocale(),
                "to" => $username,
                "subject" => "Te facilitamos tus datos de acceso",
                "content" =>
                $this->renderView("templates/emails/member/register_member.html.twig", array(
                    "text" => "Accede al siguiente link para activar tu cuenta:",
                    "url" => $this->generateUrl('app_frontend_member_apiactivate', array(
                        'secretcode' => $meta->getEmailSecretCode(),
                    )),
                )),
            );
            $manager_mail->templeteOne($value);
            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Comprueba tu correo!",
            ));

        } else {
            $response->setData(array(
                'response' => 'error',
                'msg' => "Asegurate de que tu cuenta no esté ya activada.",
            ));
        }

        return $response;
    }

    /**
     * @Route("/{_locale}/api/member/recuperar-password")
     */
    public function apiRecuperarPasswordAction(Request $request, \Swift_Mailer $mailer)
    {
        $manager_mail = $this->get("app.manager_mail");
        $helper = $this->get("app.helper");

        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('recuperarEmail');

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "Hubo un error al enviar el email de recuperación, vuelve a intentarlo más tarde.",
        ));

        // Comprovar si el email existe
        $member = $this->getDoctrine()->getManager()->getRepository('AppBundle:Member')->findOneBy(
            array('username' => $username, 'validated' => '1', 'deleted' => '0')
        );

        if ($member) {
            //Cifrar la password
            $meta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('idMember' => $member->getIdMember()));

            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $urlCode = hash('sha512', 'memberactivationoffing' . $member->getIdMember());
            $url = hash('sha512', $urlCode . $random_salt);
            $meta->setEmailSecretCode($url);
            $em->persist($meta);
            $em->flush();

            $helper->memberActivity($em, 'password_recovery_email', $member->getIdMember());

            $value = array(
                "em" => $em,
                "localeMember" => $request->getLocale(),
                "to" => $username,
                "subject" => "Te facilitamos tus datos de acceso",
                "content" =>
                $this->renderView("templates/emails/member/register_member.html.twig", array(
                    "text" => "Accede al siguiente link para recuperar la contraseña:",
                    "url" => $this->generateUrl('app_frontend_member_passwordrecovery', array(
                        'secretcode' => $meta->getEmailSecretCode(),
                    )),
                )),
            );
            $manager_mail->templeteOne($value);
            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Comprueba tu correo!",
            ));
            return $response;
        }
        return $response;
    }

    /**
     * @Route("/{_locale}/member/recovery/{secretcode}")
     */
    public function passwordRecoveryAction(Request $request, $secretcode, \Swift_Mailer $mailer)
    {
        $manager_mail = $this->get("app.manager_mail");

        $em = $this->getDoctrine()->getManager();
        $validado = 0;
        $expired = 0;
        $username = "";
        $memberMeta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(array('emailSecretCode' => $secretcode));
        if ($memberMeta) {
            $member = $em->getRepository('AppBundle:Member')->findOneBy(array('idMember' => $memberMeta->getIdMember()->getIdMember()));

            if ($member) {
                $username = $member->getUsername();
                $memberActivity = $em->getRepository('AppBundle:MemberActivity')->findOneBy(
                    array('idMember' => $member->getIdMember(), "keyActivity" => "password_recovery_email")
                );
                if ($memberActivity) {
                    $now = new DateTime('now');
                    $requested = $memberActivity->getFecha(1);
                    $requested = $requested->getTimestamp();
                    $now = $now->getTimestamp();
                    if ($now - $requested <= 259200) {
                        $memberActivity->setKeyActivity('password_recovery_email_clicked');
                        $memberMeta->setEmailSecretCode(null);
                        $em->persist($memberMeta);
                        $em->flush();
                        $em->persist($memberActivity);
                        $em->flush();

                        $validado = 1;
                    } else {
                        $memberActivity->setKeyActivity('password_recovery_email_expired');
                        $em->persist($memberActivity);
                        $em->flush();
                        $expired = 1;
                    }

                }

            }

        }

        return $this->render('frontend/member/password_recovery.html.twig', array("expired" => $expired, "validado" => $validado, "user" => $username));

    }

    /**
     * @Route("/{_locale}/api/member/recovery")
     */
    public function apiPasswordRecoveryAction(Request $request)
    {

        $helper = $this->get("app.helper");
        $manager_mail = $this->get("app.manager_mail");

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "Hubo un error al poner la nueva contraseña.",
        ));

        $em = $this->getDoctrine()->getManager();

        $password = $request->request->get('new-password');
        $passwordRepeat = $request->request->get('passwordRepeat');
        $username = $request->request->get('username');

        $member = $em->getRepository('AppBundle:Member')->findOneBy(array('username' => $username));

        if ($member) {
            //Cifrar la password
            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            $member->setSalt($random_salt);

            $passwordCode = hash('sha512', $password);
            $pwd = hash('sha512', $passwordCode . $random_salt);
            $member->setPassword($pwd);

            $em->persist($member);
            $em->flush();

            $helper->memberActivity($em, 'password_recovery_changed', $member->getIdMember());

            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Enhorabuena, tu contraseña se ha cambiado. Te redireccionamos al login!",
            ));
        }
        return $response;
    }

    /**
     * @Route("/{_locale}/api/member/social-login-register")
     */
    public function apiSocialLoginRegisterAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get("app.helper");

        $redSocial = $request->request->get('redSocial');
        $id = $request->request->get('id');
        $name = $request->request->get('name');
        $email = $request->request->get('email');
        $role = $request->request->get('role');

        if ($id && $name && $email) {

            // ROL DEL USUARIO
            if ($role != 2 && $role != 3) {
                $role = 3;
            }
            $memberRole = $em->getRepository('AppBundle:MemberRole')->findOneBy(array('idMemberRole' => $role));

            // Comprobar si el email ya existe en la ddbb
            $member = $em->getRepository('AppBundle:Member')->findOneBy(array('username' => $email));

            // Si en la ddbb no existe este id generamos un usuario con el id
            // Buscamos si el id Existe
            // Si el usuario ya existe, comprobamos si el idFacebook o idGoogle que nos pase tamien existe, si no lo actualizamos
            switch ($redSocial) {
                case 'facebook':
                    $idExist = $em->getRepository('AppBundle:Member')->findOneBy(array('keyFacebook' => $id));
                    break;
                case 'google':
                    $idExist = $em->getRepository('AppBundle:Member')->findOneBy(array('keyGoogle' => $id));
                    break;
            }

            $registrarActualizarUsuario = 0;
            // Si el email (usuario) existe entramos
            if ($member) {
                // Comprobamos si el idFacebook existe
                if (!$idExist) {
                    // Como no existe actualizamos el usuario
                    $registrarActualizarUsuario = 1;
                    $member = $em->getRepository('AppBundle:Member')->findOneBy(array('username' => $email));
                }

                // Como el email no existe comprobamos si el idFacebook existe
            } else if (!$idExist) {
                // Como no existe email ni el idFacebook creamos otro usuario
                $registrarActualizarUsuario = 1;
                $member = new Member();

                $member->setIdMemberRole($memberRole);

                $member->setName($name);
                $member->setUsername($email);
                $member->setDeleted(0);
                $member->setValidated(1);

                //Cifrar la password
                $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
                $member->setSalt($random_salt);

                $password = rand(1, 1000000);
                $passwordCode = hash('sha512', $password);
                $pwd = hash('sha512', $passwordCode . $random_salt);
                $member->setPassword($pwd);

                $em->persist($member);
                $em->flush();

                $memberMeta = new MemberMeta();
                $memberMeta->setIdMember($member->getIdMember());
                $em->persist($memberMeta);
                $em->flush();

                mkdir('member/' . $member->getIdMember());
                mkdir('member/' . $member->getIdMember() . '/img-avatar/');
            }

            // Entramos en el caso de que se tenga que actualizar o crear el usuario
            if ($registrarActualizarUsuario == 1) {

                $member->setUsername($email);

                // Guardamos el id dependiando del boton de la red
                switch ($redSocial) {
                    case 'facebook':
                        $memberMeta->setKeyFacebook($id);
                        break;
                    case 'google':
                        $memberMeta->setKeyGoogle($id);
                        break;
                }

                $em->persist($member);
                $em->flush();
                $em->persist($memberMeta);
                $em->flush();

            }
            $member = null;

            // ### Login
            switch ($redSocial) {
                case 'facebook':
                    $member = $em->getRepository('AppBundle:Member')->findOneBy(
                        array('validated' => '1', 'deleted' => '0')
                    );
                    $memberMeta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(
                        array('keyFacebook' => $id)
                    );
                    break;
                case 'google':
                    $member = $em->getRepository('AppBundle:Member')->findOneBy(
                        array('validated' => '1', 'deleted' => '0')
                    );
                    $memberMeta = $em->getRepository('AppBundle:MemberMeta')->findOneBy(
                        array('keyGoogle' => $id)
                    );
                    break;
            }

            if ($member && $memberMeta) {
                $firewall_name = "our_db_provider";
                $token = new UsernamePasswordToken($member, null, $firewall_name, $member->getRoles());
                $this->get('security.token_storage')->setToken($token);
                return new Response(1);
            } else {
                return new Response(0);
            }
        }
        return new Response(0);

    }

    /**
     * @Route("/{_locale}/api/member/validar")
     */
    public function apiValidarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $username = $request->request->get('username');
        $password = $request->request->get('password');

        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => "No se pudo iniciar sesión.",
        ));

        $member = $em->getRepository('AppBundle:Member')->findUserLogin($username);

        $salt = $member[0]['salt'];
        $password = hash('sha512', $password);
        $pwd = hash('sha512', $password . $salt);

        $member = $em->getRepository('AppBundle:Member')->findUserLoginDos($username, $pwd);

        $member = $this->getDoctrine()->getManager()->getRepository('AppBundle:Member')->findOneBy(
            array('username' => $username, 'password' => $pwd, 'validated' => 1, 'deleted' => 0)
        );

        if ($member) {
            $firewall_name = "our_db_provider";
            $token = new UsernamePasswordToken($member, null, $firewall_name, $member->getRoles());
            $this->get('security.token_storage')->setToken($token);

            $response->setStatusCode(200);
            $response->setData(array(
                'response' => 'success',
                'msg' => "Has iniciado sesión correctamente.",
            ));
        }
        
        return $response;
    }

}

<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{

    /**
     * @Route("/{_locale}/product")
     */
    public function buscadorAction(Request $request)
    {
        $prod = $this->get("app.services_product");

        $em = $this->getDoctrine()->getManager();

        $localeProduct = $request->get("localeProduct");
        $filtreMinPrice = $request->get("filtreMinPrice");
        $filtreMaxPrice = $request->get("filtreMaxPrice");
        $filtreCategory = $request->get("category");
  
        if (!$localeProduct) {
            $localeProduct = $request->getLocale();
        }
        $valores = array(
            "em" => $em,
            "localeProduct" => $localeProduct,
            "idProduct" => null,
            "filtreMinPrice" => $filtreMinPrice,
            "filtreMaxPrice" => $filtreMaxPrice,
            "filtreCategory" => $filtreCategory,
        );
        $product = $prod->searchProduct($valores);
        
        //if(!$min_price && !$max_price){
            //$min_price = $em->getRepository('AppBundle:Product')->searchMinPrice();
            //$max_price = $em->getRepository('AppBundle:Product')->searchMaxPrice();
        //}
        //Aplicació del bundle KnpPaginator
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $product, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $category = $em->getRepository('AppBundle:Category')->findBy(array('categoryKey' => 'product_level_1'));

        return $this->render('frontend/product/buscador.html.twig',array(
            'pagination' => $pagination,
            'filtreMinPrice' => $filtreMinPrice,
            'filtreMaxPrice' => $filtreMaxPrice,
            'category' => $category,
        ));
    }
}

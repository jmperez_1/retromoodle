<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/*
 * ProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends EntityRepository
{
    public function searchProduct($valores)
    {
        $locale = $valores['localeProduct'];
        $idProduct = $valores['idProduct'];
        $filtreMinPrice = $valores['filtreMinPrice'];
        $filtreMaxPrice = $valores['filtreMaxPrice'];

        $sqlWhere = ' WHERE 1 = 1 ';
        $sqlAnd = '';
        $sqlSelect = 'SELECT ';
        $sqlColumnas = '
            p.idProduct,
            p.price,
            p.stock,
            p.deleted,
            p.visible,
            p.category,
            p.photos,
            pm.locale,
            pm.name,
            pm.url
        ';

        $sqlFrom = ' FROM AppBundle:Product p ';
        $sqlJoin = ' JOIN AppBundle:ProductMeta pm WITH p.idProduct = pm.idProduct ';

        $sqlAnd .= " AND pm.locale LIKE '" . $locale . "' ";

        if ($idProduct){
            $sqlAnd .= " AND p.idProduct = " . $idProduct . " ";
        }
        if($filtreMinPrice && $filtreMaxPrice){
            $sqlAnd .= " AND p.price BETWEEN " . $filtreMinPrice . " AND " . $filtreMaxPrice . " ";
        }

        $sentencia = $sqlSelect . $sqlColumnas . $sqlFrom . $sqlJoin . $sqlWhere . $sqlAnd;
        return $this->getEntityManager()->createQuery($sentencia)->getResult();
    }
}

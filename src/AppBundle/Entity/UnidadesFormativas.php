<?php

namespace AppBundle\Entity;

/**
 * UnidadesFormativas
 */
class UnidadesFormativas
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $horas;

    /**
     * @var \AppBundle\Entity\Modulos
     */
    private $nombreModulo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dniAlumno;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dniAlumno = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set horas
     *
     * @param integer $horas
     *
     * @return UnidadesFormativas
     */
    public function setHoras($horas)
    {
        $this->horas = $horas;

        return $this;
    }

    /**
     * Get horas
     *
     * @return integer
     */
    public function getHoras()
    {
        return $this->horas;
    }

    /**
     * Set nombreModulo
     *
     * @param \AppBundle\Entity\Modulos $nombreModulo
     *
     * @return UnidadesFormativas
     */
    public function setNombreModulo(\AppBundle\Entity\Modulos $nombreModulo = null)
    {
        $this->nombreModulo = $nombreModulo;

        return $this;
    }

    /**
     * Get nombreModulo
     *
     * @return \AppBundle\Entity\Modulos
     */
    public function getNombreModulo()
    {
        return $this->nombreModulo;
    }

    /**
     * Add dniAlumno
     *
     * @param \AppBundle\Entity\Usuarios $dniAlumno
     *
     * @return UnidadesFormativas
     */
    public function addDniAlumno(\AppBundle\Entity\Usuarios $dniAlumno)
    {
        $this->dniAlumno[] = $dniAlumno;

        return $this;
    }

    /**
     * Remove dniAlumno
     *
     * @param \AppBundle\Entity\Usuarios $dniAlumno
     */
    public function removeDniAlumno(\AppBundle\Entity\Usuarios $dniAlumno)
    {
        $this->dniAlumno->removeElement($dniAlumno);
    }

    /**
     * Get dniAlumno
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDniAlumno()
    {
        return $this->dniAlumno;
    }
}


<?php

namespace AppBundle\Entity;

/**
 * Modulos
 */
class Modulos
{
    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $horas;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $dniProfe;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dniProfe = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set horas
     *
     * @param integer $horas
     *
     * @return Modulos
     */
    public function setHoras($horas)
    {
        $this->horas = $horas;

        return $this;
    }

    /**
     * Get horas
     *
     * @return integer
     */
    public function getHoras()
    {
        return $this->horas;
    }

    /**
     * Add dniProfe
     *
     * @param \AppBundle\Entity\Usuarios $dniProfe
     *
     * @return Modulos
     */
    public function addDniProfe(\AppBundle\Entity\Usuarios $dniProfe)
    {
        $this->dniProfe[] = $dniProfe;

        return $this;
    }

    /**
     * Remove dniProfe
     *
     * @param \AppBundle\Entity\Usuarios $dniProfe
     */
    public function removeDniProfe(\AppBundle\Entity\Usuarios $dniProfe)
    {
        $this->dniProfe->removeElement($dniProfe);
    }

    /**
     * Get dniProfe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDniProfe()
    {
        return $this->dniProfe;
    }
}


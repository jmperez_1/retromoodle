<?php

namespace AppBundle\Entity;

/**
 * Usuarios
 */
class Usuarios
{
    /**
     * @var string
     */
    private $dni;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var string
     */
    private $apellido1;

    /**
     * @var string
     */
    private $apellido2;

    /**
     * @var \AppBundle\Entity\Roles
     */
    private $rol;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $nombreModulo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $nombreUf;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nombreModulo = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nombreUf = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Usuarios
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuarios
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido1
     *
     * @param string $apellido1
     *
     * @return Usuarios
     */
    public function setApellido1($apellido1)
    {
        $this->apellido1 = $apellido1;

        return $this;
    }

    /**
     * Get apellido1
     *
     * @return string
     */
    public function getApellido1()
    {
        return $this->apellido1;
    }

    /**
     * Set apellido2
     *
     * @param string $apellido2
     *
     * @return Usuarios
     */
    public function setApellido2($apellido2)
    {
        $this->apellido2 = $apellido2;

        return $this;
    }

    /**
     * Get apellido2
     *
     * @return string
     */
    public function getApellido2()
    {
        return $this->apellido2;
    }

    /**
     * Set rol
     *
     * @param \AppBundle\Entity\Roles $rol
     *
     * @return Usuarios
     */
    public function setRol(\AppBundle\Entity\Roles $rol = null)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return \AppBundle\Entity\Roles
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Add nombreModulo
     *
     * @param \AppBundle\Entity\Modulos $nombreModulo
     *
     * @return Usuarios
     */
    public function addNombreModulo(\AppBundle\Entity\Modulos $nombreModulo)
    {
        $this->nombreModulo[] = $nombreModulo;

        return $this;
    }

    /**
     * Remove nombreModulo
     *
     * @param \AppBundle\Entity\Modulos $nombreModulo
     */
    public function removeNombreModulo(\AppBundle\Entity\Modulos $nombreModulo)
    {
        $this->nombreModulo->removeElement($nombreModulo);
    }

    /**
     * Get nombreModulo
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNombreModulo()
    {
        return $this->nombreModulo;
    }

    /**
     * Add nombreUf
     *
     * @param \AppBundle\Entity\UnidadesFormativas $nombreUf
     *
     * @return Usuarios
     */
    public function addNombreUf(\AppBundle\Entity\UnidadesFormativas $nombreUf)
    {
        $this->nombreUf[] = $nombreUf;

        return $this;
    }

    /**
     * Remove nombreUf
     *
     * @param \AppBundle\Entity\UnidadesFormativas $nombreUf
     */
    public function removeNombreUf(\AppBundle\Entity\UnidadesFormativas $nombreUf)
    {
        $this->nombreUf->removeElement($nombreUf);
    }

    /**
     * Get nombreUf
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNombreUf()
    {
        return $this->nombreUf;
    }
}


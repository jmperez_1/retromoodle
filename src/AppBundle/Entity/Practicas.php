<?php

namespace AppBundle\Entity;

/**
 * Practicas
 */
class Practicas
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nombre;

    /**
     * @var integer
     */
    private $nota;

    /**
     * @var \AppBundle\Entity\UnidadesFormativas
     */
    private $nombreUf;

    /**
     * @var \AppBundle\Entity\Usuarios
     */
    private $dniAlumno;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Practicas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nota
     *
     * @param integer $nota
     *
     * @return Practicas
     */
    public function setNota($nota)
    {
        $this->nota = $nota;

        return $this;
    }

    /**
     * Get nota
     *
     * @return integer
     */
    public function getNota()
    {
        return $this->nota;
    }

    /**
     * Set nombreUf
     *
     * @param \AppBundle\Entity\UnidadesFormativas $nombreUf
     *
     * @return Practicas
     */
    public function setNombreUf(\AppBundle\Entity\UnidadesFormativas $nombreUf = null)
    {
        $this->nombreUf = $nombreUf;

        return $this;
    }

    /**
     * Get nombreUf
     *
     * @return \AppBundle\Entity\UnidadesFormativas
     */
    public function getNombreUf()
    {
        return $this->nombreUf;
    }

    /**
     * Set dniAlumno
     *
     * @param \AppBundle\Entity\Usuarios $dniAlumno
     *
     * @return Practicas
     */
    public function setDniAlumno(\AppBundle\Entity\Usuarios $dniAlumno = null)
    {
        $this->dniAlumno = $dniAlumno;

        return $this;
    }

    /**
     * Get dniAlumno
     *
     * @return \AppBundle\Entity\Usuarios
     */
    public function getDniAlumno()
    {
        return $this->dniAlumno;
    }
}


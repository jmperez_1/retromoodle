<?php

namespace AppBundle\Entity;

/**
 * Roles
 */
class Roles
{
    /**
     * @var string
     */
    private $nombre;


    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}


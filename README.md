Bundle Mode Manteniment -  LexikMaintenanceBundle
Documentació:
  https://github.com/lexik/LexikMaintenanceBundle
Instal·lació i configuració:
  https://github.com/lexik/LexikMaintenanceBundle/blob/master/Resources/doc/index.md

Activació mode manteniment:
  
  php bin/console lexik:maintenance:lock

Desactivació mode manteniment:
  
  php bin/console lexik:maintenance:unlock
